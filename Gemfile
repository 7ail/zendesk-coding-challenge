# frozen_string_literal: true

source 'https://rubygems.org'

ruby                              '2.6.3'

gem 'bcrypt',                     '3.1.13'
gem 'bootsnap',                   '1.4.4', require: false
gem 'bootstrap-sass',             '3.4.1'
gem 'coffee-rails',               '5.0.0'
gem 'jbuilder',                   '2.9.1'
gem 'jquery-rails',               '4.3.3'
gem 'puma',                       '3.12.1'
gem 'rails',                      '5.2.3'
gem 'sass-rails',                 '5.0.7'
gem 'turbolinks',                 '5.2.0'
gem 'uglifier',                   '4.1.20'
gem 'will_paginate',              '3.1.7'

group :development, :test do
  gem 'dotenv-rails',             '2.7.2'
  gem 'pry',                      '0.12.2'
  gem 'rails-controller-testing', '1.0.4'
  gem 'rspec-rails',              '3.8.2'
  gem 'rubocop-performance',      '1.3.0'
  gem 'rubocop-rails',            '2.0.0'
  gem 'solargraph',               '0.32.5'
  gem 'sqlite3',                  '1.4.1'
end

group :development do
  gem 'listen',                   '3.1.5'
  gem 'spring',                   '2.0.2'
  gem 'spring-watcher-listen',    '2.0.1'
  gem 'web-console',              '3.7.0'
end

group :test do
  gem 'faker',                    '1.9.3'
  gem 'webmock',                  '3.6.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
