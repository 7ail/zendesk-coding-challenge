# GETTING STARTED

### Requirements
* **Ruby** 2.6.3
* **Rails** 5.2.3
---

### Installation
```zsh
git clone https://gitlab.com/7ail/zendesk-coding-challenge.git
cd zendesk-coding-challenge
bundle install
```
---

### ENV file
##### Requirements
* Be in the local repository folder
* Zendesk account
  - replace \<SECRET\> below with your Zendesk credentials
##### CLI commands
```zsh
cd zendesk-coding-challenge
touch .env
echo 'ZENDESK_SUBDOMAIN = "<SECRET>"' >> .env
echo 'ZENDESK_URL = "https://$ZENDESK_SUBDOMAIN.zendesk.com"' >> .env
echo 'ZENDESK_USERNAME = "<SECRET>"' >> .env
echo 'ZENDESK_PASSWORD = "<SECRET>"' >> .env
echo 'ZENDESK_TICKETS_PER_PAGE = 25' >> .env
```
---

### Run Program
```zsh
rails s
```
##### Result
```zsh
=> Booting Puma
=> Rails 5.2.3 application starting in development
Puma starting in single mode...
* Version 3.12.1 (ruby 2.6.3-p62), codename: Llamas in Pajamas
* Min threads: 5, max threads: 5
* Environment: development
* Listening on tcp://localhost:3000
Use Ctrl-C to stop
```
##### Website
* [http://localhost:3000/](http://localhost:3000/)
---

### Testing
```zsh
rspec
```
---

### Linter
```zsh
rubocop
```
---

### Documentation
```zsh
rspec --format d
```
---

### High Level Design

View <--> Controller <--> Zendesk::API

##### Important Files
* ZendeskController (app/controllers/zendesk_controller.rb)
* Zendesk::API (lib/zendesk/api.rb)
* HTTP::Get (lib/http/get.rb)
* Custom Errors (lib/errors/)


##### Included Test
* Controller Test
* Routing Test
* Zendesk::API Test
* HTTP::Get Test
---


# TODO
##### Testing
* View Test
---
