# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../../lib/zendesk/api.rb'
require_relative '../zendesk_helper.rb'

RSpec.describe Zendesk::API do
  context 'show ticket' do
    context 'subdomain' do
      context 'valid' do
        before do
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets/1.json")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 200, body: ticket_data.to_json)
        end

        it 'should not raise an error' do
          expect { Zendesk::API.new.show_ticket('1') }.to_not raise_error
        end
      end

      context 'invalid' do
        before do
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets/1.json")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 404, body: subdomain_invalid_data.to_json)
        end

        it 'should raise an error' do
          expect { Zendesk::API.new.show_ticket('1') }.to raise_error(Errors::ZendeskError)
        end

        context 'error' do
          it 'should have 404 status' do
            Zendesk::API.new.show_ticket('1')
          rescue Errors::ZendeskError => e
            expect(e.status).to match(404)
          end

          it 'should have Zendesk error' do
            Zendesk::API.new.show_ticket('1')
          rescue Errors::ZendeskError => e
            expect(e.error).to match('Zendesk')
          end

          it 'should have a message' do
            Zendesk::API.new.show_ticket('1')
          rescue Errors::ZendeskError => e
            expect(e.message).to_not be_nil
          end
        end
      end
    end

    context 'basic authentication' do
      context 'valid' do
        before do
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets/1.json")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 200, body: ticket_data.to_json)
        end

        it 'should not raise an error' do
          expect { Zendesk::API.new.show_ticket('1') }.to_not raise_error
        end
      end

      context 'invalid' do
        before do
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets/1.json")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 401, body: authentication_failed_data.to_json)
        end

        it 'should raise an error' do
          expect { Zendesk::API.new.show_ticket('1') }.to raise_error(Errors::ZendeskError)
        end

        context 'error' do
          it 'should have 401 status' do
            Zendesk::API.new.show_ticket('1')
          rescue Errors::ZendeskError => e
            expect(e.status).to match(401)
          end

          it 'should have Unexpected Zendesk Response error' do
            Zendesk::API.new.show_ticket('1')
          rescue Errors::ZendeskError => e
            expect(e.error).to match('Zendesk')
          end

          it 'should have a message' do
            Zendesk::API.new.show_ticket('1')
          rescue Errors::ZendeskError => e
            expect(e.message).to_not be_nil
          end
        end
      end
    end

    context 'parameters' do
      before do
        stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets/1.json")
          .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
          .to_return(status: 200, body: ticket_data.to_json)
      end

      it 'should only accept 1 variables' do
        expect { Zendesk::API.new.show_ticket }.to raise_error(ArgumentError)
        expect { Zendesk::API.new.show_ticket('1', '2') }.to raise_error(ArgumentError)
        expect { Zendesk::API.new.show_ticket('1') }.to_not raise_error
      end

      context 'invalid' do
        it 'should raise an error if not of type Integer' do
          expect { Zendesk::API.new.show_ticket('what') }.to raise_error(Errors::ZendeskError)
        end

        context 'error' do
          it 'should have 500 status' do
            Zendesk::API.new.show_ticket('what')
          rescue Errors::ZendeskError => e
            expect(e.status).to match(500)
          end

          it 'should have Internal Error error' do
            Zendesk::API.new.show_ticket('what')
          rescue Errors::ZendeskError => e
            expect(e.error).to match('Internal Error')
          end

          it 'should have a message' do
            Zendesk::API.new.show_ticket('what')
          rescue Errors::ZendeskError => e
            expect(e.message).to_not be_nil
          end
        end
      end
    end

    context 'instance variables' do
      before do
        stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets/1.json")
          .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
          .to_return(status: 200, body: ticket_data.to_json)

        @response = Zendesk::API.new
        @response.show_ticket('1')
      end

      it 'should be able to access response code' do
        expect { @response.code }.to_not raise_error
      end

      it 'should be able to acess response body' do
        expect { @response.body }.to_not raise_error
      end
    end

    context 'response' do
      context 'valid' do
        before do
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets/1.json")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 200, body: ticket_data.to_json)

          @response = Zendesk::API.new
          @response.show_ticket('1')
        end

        context 'code' do
          it 'should be 200' do
            expect(@response.code).to match(200)
          end
        end

        context 'body' do
          before { @body = @response.body }

          it 'should not be nil' do
            expect(@response.body).to_not be_nil
          end

          context 'ticket' do
            it 'should exist' do
              expect(@response.body['ticket']).to_not be_nil
            end
          end
        end
      end

      context 'invalid (id)' do
        before do
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets/1000.json")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 404, body: empty_ticket_data.to_json)
        end

        it 'should raise an error' do
          expect { Zendesk::API.new.show_ticket('1000') }.to raise_error(Errors::ZendeskError)
        end

        context 'error' do
          it 'should have 404 status' do
            Zendesk::API.new.show_ticket('1000')
          rescue Errors::ZendeskError => e
            expect(e.status).to match(404)
          end

          it 'should Zendesk error' do
            Zendesk::API.new.show_ticket('1000')
          rescue Errors::ZendeskError => e
            expect(e.error).to match('Zendesk')
          end

          it 'should have a message' do
            Zendesk::API.new.show_ticket('1000')
          rescue Errors::ZendeskError => e
            expect(e.message).to_not be_nil
          end
        end
      end
    end
  end
end
