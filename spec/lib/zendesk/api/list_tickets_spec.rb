# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../../lib/zendesk/api.rb'
require_relative '../zendesk_helper.rb'

RSpec.describe Zendesk::API do
  context 'list tickets' do
    context 'subdomain' do
      context 'valid' do
        before do
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets.json?page=1&per_page=1")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 200, body: ticket_list_data.to_json)
        end

        it 'should not raise an error' do
          expect { Zendesk::API.new.list_tickets('1', '1') }.to_not raise_error
        end
      end

      context 'invalid' do
        before do
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets.json?page=1&per_page=1")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 404, body: subdomain_invalid_data.to_json)
        end

        it 'should raise an error' do
          expect { Zendesk::API.new.list_tickets('1', '1') }.to raise_error(Errors::ZendeskError)
        end

        context 'error' do
          it 'should have 404 status' do
            Zendesk::API.new.list_tickets('1', '1')
          rescue Errors::ZendeskError => e
            expect(e.status).to match(404)
          end

          it 'should have Zendesk error' do
            Zendesk::API.new.list_tickets('1', '1')
          rescue Errors::ZendeskError => e
            expect(e.error).to match('Zendesk')
          end

          it 'should have a message' do
            Zendesk::API.new.list_tickets('1', '1')
          rescue Errors::ZendeskError => e
            expect(e.message).to_not be_nil
          end
        end
      end
    end

    context 'basic authentication' do
      context 'valid' do
        before do
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets.json?page=1&per_page=1")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 200, body: ticket_list_data.to_json)
        end

        it 'should not raise an error' do
          expect { Zendesk::API.new.list_tickets('1', '1') }.to_not raise_error
        end
      end

      context 'invalid' do
        before do
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets.json?page=1&per_page=1")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 401, body: authentication_failed_data.to_json)
        end

        it 'should raise an error' do
          expect { Zendesk::API.new.list_tickets('1', '1') }.to raise_error(Errors::ZendeskError)
        end

        context 'error' do
          it 'should have 401 status' do
            Zendesk::API.new.list_tickets('1', '1')
          rescue Errors::ZendeskError => e
            expect(e.status).to match(401)
          end

          it 'should have Zendesk error' do
            Zendesk::API.new.list_tickets('1', '1')
          rescue Errors::ZendeskError => e
            expect(e.error).to match('Zendesk')
          end

          it 'should have a message' do
            Zendesk::API.new.list_tickets('1', '1')
          rescue Errors::ZendeskError => e
            expect(e.message).to_not be_nil
          end
        end
      end
    end

    context 'parameters' do
      before do
        stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets.json?page=1&per_page=1")
          .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
          .to_return(status: 200, body: ticket_list_data.to_json)
      end

      it 'should only accept 2 variables' do
        expect { Zendesk::API.new.list_tickets('1') }.to raise_error(ArgumentError)
        expect { Zendesk::API.new.list_tickets('1', '2', '3') }.to raise_error(ArgumentError)
        expect { Zendesk::API.new.list_tickets('1', '1') }.to_not raise_error
      end

      context 'invalid' do
        it 'should raise an error if not of type Integer' do
          expect { Zendesk::API.new.list_tickets('what', '1') }.to raise_error(Errors::ZendeskError)
          expect { Zendesk::API.new.list_tickets('1', 'what') }.to raise_error(Errors::ZendeskError)
        end

        context 'error' do
          it 'should have 500 status' do
            Zendesk::API.new.list_tickets('what', '1')
          rescue Errors::ZendeskError => e
            expect(e.status).to match(500)
          end

          it 'should have Internal Error error' do
            Zendesk::API.new.list_tickets('what', '1')
          rescue Errors::ZendeskError => e
            expect(e.error).to match('Internal Error')
          end

          it 'should have a message' do
            Zendesk::API.new.list_tickets('what', '1')
          rescue Errors::ZendeskError => e
            expect(e.message).to_not be_nil
          end
        end
      end
    end

    context 'instance variables' do
      before do
        stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets.json?page=1&per_page=1")
          .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
          .to_return(status: 200, body: ticket_list_data.to_json)

        @response = Zendesk::API.new
        @response.list_tickets('1', '1')
      end

      it 'should be able to access response code' do
        expect { @response.code }.to_not raise_error
      end

      it 'should be able to acess response body' do
        expect { @response.body }.to_not raise_error
      end
    end

    context 'response' do
      context 'valid' do
        before do
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets.json?page=1&per_page=1")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 200, body: ticket_list_data.to_json)

          @response = Zendesk::API.new
          @response.list_tickets('1', '1')
        end

        context 'code' do
          it 'should be 200' do
            expect(@response.code).to match(200)
          end
        end

        context 'body' do
          before { @body = @response.body }

          it 'should not be nil' do
            expect(@response.body).to_not be_nil
          end

          context 'tickets' do
            it 'should exist' do
              expect(@response.body['tickets']).to_not be_nil
            end

            it 'should be an Array' do
              expect(@response.body['tickets']).to be_an(Array)
            end

            it 'should not be empty' do
              expect(@response.body['tickets']).to_not be_empty
            end
          end

          context 'count' do
            it 'should exist' do
              expect(@response.body['count']).to_not be_nil
            end

            it 'should be an Integer' do
              expect(@response.body['count']).to be_an(Integer)
            end
          end
        end
      end

      context 'invalid (empty tickets array)' do
        before do
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets.json?page=1&per_page=1")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 200, body: empty_ticket_list_data.to_json)
        end

        it 'should raise an error' do
          expect { Zendesk::API.new.list_tickets('1', '1') }.to raise_error(Errors::ZendeskError)
        end

        context 'error' do
          it 'should have 204 status' do
            Zendesk::API.new.list_tickets('1', '1')
          rescue Errors::ZendeskError => e
            expect(e.status).to match(204)
          end

          it 'should have Zendesk error' do
            Zendesk::API.new.list_tickets('1', '1')
          rescue Errors::ZendeskError => e
            expect(e.error).to match('Zendesk')
          end

          it 'should have a message' do
            Zendesk::API.new.list_tickets('1', '1')
          rescue Errors::ZendeskError => e
            expect(e.message).to_not be_nil
          end
        end
      end

      context 'invalid (code != 200)' do
        before do
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets.json?page=1&per_page=1")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 400, body: {}.to_json)
        end

        it 'should raise an error' do
          expect { Zendesk::API.new.list_tickets('1', '1') }.to raise_error(Errors::ZendeskError)
        end

        context 'error' do
          it 'should have response\'s status' do
            Zendesk::API.new.list_tickets('1', '1')
          rescue Errors::ZendeskError => e
            expect(e.status).to match(400)
          end

          it 'should have Unexpected Zendesk Response error' do
            Zendesk::API.new.list_tickets('1', '1')
          rescue Errors::ZendeskError => e
            expect(e.error).to match('Unexpected Zendesk Response')
          end

          it 'should have a message' do
            Zendesk::API.new.list_tickets('1', '1')
          rescue Errors::ZendeskError => e
            expect(e.message).to_not be_nil
          end
        end
      end
    end
  end
end
