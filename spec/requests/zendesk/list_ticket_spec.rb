# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ZendeskController, type: :request do
  describe 'GET list_tickets' do
    before { @valid_params = { 'page' => '1' } }

    context 'route test' do
      before do
        stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets.json?page=#{@valid_params['page']}" \
          "&per_page=#{ENV['ZENDESK_TICKETS_PER_PAGE']}")
          .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
          .to_return(status: 200, body: ticket_list_data.to_json)
        send_request
      end

      context 'status code' do
        it 'returns http success' do
          expect(response).to have_http_status(:success)
        end
      end

      context 'view' do
        it 'should render list_tickets template' do
          expect(response).to render_template('list_tickets')
        end
      end
    end
  end

  def send_request(params = @valid_params)
    get zendesk_list_tickets_path, params: params
  end
end
