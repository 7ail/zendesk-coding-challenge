# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ZendeskController, type: :request do
  describe 'GET show_ticket' do
    before { @valid_params = { 'id' => '1' } }

    context 'route test' do
      before do
        stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets/#{@valid_params['id']}.json")
          .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
          .to_return(status: 200, body: ticket_data.to_json)
        send_request
      end

      context 'status code' do
        it 'returns http success' do
          expect(response).to have_http_status(:success)
        end
      end

      context 'view' do
        it 'should render show_ticket template' do
          expect(response).to render_template('show_ticket')
        end
      end
    end
  end

  def send_request(params = @valid_params)
    get zendesk_show_ticket_path(params['id'])
  end
end
