# frozen_string_literal: true

class ZendeskController < ApplicationController
  include Zendesk

  before_action :initialize_zendesk_api

  def list_tickets
    @response.list_tickets(params['page'], ENV['ZENDESK_TICKETS_PER_PAGE'])
  rescue Errors::ZendeskError => e
    handle_error(e)
  end

  def show_ticket
    @response.show_ticket(params['id'])
  rescue Errors::ZendeskError => e
    handle_error(e)
  end

  private

  def initialize_zendesk_api
    @response = Zendesk::API.new
  end

  def handle_error(object)
    @alert = true
    @error = object.error
    @code = object.status
    @message = object.message
  end
end
